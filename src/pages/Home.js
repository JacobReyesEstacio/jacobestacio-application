import React from 'react'
import logoreactproject from '../images/logoreactproject.png'
import { Button, Card, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import chocomousecake from "../images/chocomousecake.jpg";
import classicdonut12 from "../images/classicdonut12.jpg";
import strawberryshortcake from "../images/strawberryshortcake.jpg";
import caramellemoncake from "../images/caramellemoncake.jpg";

function Home() {






  return (
    <div className='home-container'>
    <div className='logo'>
      <img className='logoImg' src={logoreactproject} alt='LOGO' />
    </div>
    <div className='description-container'>
    <h1 className='description'>About us</h1>
    <h3 className='description pt-3'>
Welcome to Pâtisserie Flourique, where our passion for crafting delectable sweets knows no bounds! Nestled in the heart of Manila, our bakeshop is a sweet haven for those with an insatiable craving for the finest pastries and confections. With every bite, we invite you to embark on a sensory journey that will transport you to a world of flavor and delight.</h3>
    </div>
    <div className='featured-products-container'>
    <h1 className='description pt-5'>Must try!!</h1>
    <Row noGutters lg={4}>
                <div className="d-flex justify-content-center cardContainer">
                   <Card className="h-100 p-3 align-items-center cardMain"  style={{ gap: ".2rem" }} 
                    as={Link} to={`/products/`}>
                   <Card.Img variant="top" className="view-image" src={caramellemoncake} height={"200px"} style={{ objectFit: "cover" }} />
                    <Card.Body className="d-flex flex-column">
                    <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
                    <span className="fs-7 card-text">Caramel Lemon Cake</span>
                    </Card.Title>                    
                  </Card.Body>
                </Card>
                </div>
                <div className="d-flex justify-content-center cardContainer">
                   <Card className="h-100 p-3 align-items-center cardMain"  style={{ gap: ".2rem" }} 
                    as={Link} to={`/products/`}>
                   <Card.Img variant="top" className="view-image" src={strawberryshortcake} height={"200px"} style={{ objectFit: "cover" }} />
                    <Card.Body className="d-flex flex-column">
                    <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
                    <span className="fs-7 card-text">Strawberry Shortcake</span>
                    </Card.Title>                    
                  </Card.Body>
                </Card>
                </div>
                <div className="d-flex justify-content-center cardContainer">
                   <Card className="h-100 p-3 align-items-center cardMain"  style={{ gap: ".2rem" }} 
                    as={Link} to={`/products/`}>
                   <Card.Img variant="top" className="view-image" src={classicdonut12} height={"200px"} style={{ objectFit: "cover" }} />
                    <Card.Body className="d-flex flex-column">
                    <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
                    <span className="fs-7 card-text">Dozen Classic Donuts</span>
                    </Card.Title>                    
                  </Card.Body>
                </Card>
                </div>
                <div className="d-flex justify-content-center cardContainer">
                   <Card className="h-100 p-3 align-items-center cardMain"  style={{ gap: ".2rem" }} 
                    as={Link} to={`/products/`}>
                   <Card.Img variant="top" className="view-image" src={chocomousecake} height={"200px"} style={{ objectFit: "cover" }} />
                    <Card.Body className="d-flex flex-column">
                    <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
                    <span className="fs-7 card-text">Chocolate Mouse</span>
                    </Card.Title>                    
                  </Card.Body>
                </Card>
                </div>

</Row>
<div>
  <h2 className='description p-5'>"Indulge in Sweet Ecstasy at Pâtisserie Flourique: Where Every Bite Takes You on a Flavorful Journey!"</h2>
</div>
    </div>
  
   
    </div>
  )
}

export default Home
