import { useState, useEffect } from 'react';
import { Container, Card, Row, Col, Button } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import AddToCart from '../components/AddToCart';
import { useCartData } from '../CartContext';
import axios from 'axios';
import general from '../images/general.jpg'
import caramellemoncake from "../images/caramellemoncake.jpg";
import chococake from "../images/chococake.jpg"
import chococookie6 from "../images/chococookie6.jpg";
import chocomousecake from "../images/chocomousecake.jpg";
import classicdonut12 from "../images/classicdonut12.jpg";
import crepecake from "../images/crepecake.jpg";
import donut6 from "../images/donut6.jpg";
import normalcookie6 from "../images/normalcookie6.jpg";
import SberryDonut from "../images/SberryDonut.jpg";
import strawberryshortcake from "../images/strawberryshortcake.jpg";
import strawblueberryCake from "../images/strawblueberryCake.jpg";
import weddingcake from "../images/weddingcake.jpg"


// ...

export default function ProductView() {
	let str = "\u20B1";
	const { productId } = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [currentQuantity, setCurrentQuantity] = useState(0);
	const [image, setImage] = useState("");
	const cartData = useCartData();
	const token = localStorage.getItem("token");
  
	useEffect(() => {
	  const cartItem = cartData.find((item) => item.productId === productId);
	  setCurrentQuantity(cartItem ? cartItem.quantity : 0); // Set the initial quantity
	}, [productId]);
  
	useEffect(() => {
	  console.log(productId);
  
	  fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/${productId}`)
		.then((res) => res.json())
		.then((data) => {
		  console.log(data);
  
		  setName(data.productName);
		  setDescription(data.productDescription);
		  setPrice(data.productPrice);
		});
	}, [productId]);


	const getImagePath = (productId) => {
		const productImage = imageArray.find((item) => item.id === productId);
		return productImage ? productImage.image : general;
	  };
	
	  useEffect(() => {
		// Call the getImagePath function to get the image path based on product ID
		const imagePath = getImagePath(productId);
	   setImage(imagePath);
	  }, [productId]);
	
	 const imageArray = [
		{ id: "650ad4444e6cf18f78d4edf8", image: caramellemoncake },
		{ id: "650ad45d4e6cf18f78d4ef50", image: chococake },
		{ id: "650ad4804e6cf18f78d4f19a", image: chococookie6 },
		{ id: "650ad4994e6cf18f78d4f39b", image: chocomousecake },
		{ id: "650ad4b84e6cf18f78d4f69c", image: classicdonut12 },
		{ id: "650ad4ce4e6cf18f78d4f904", image: crepecake },
		{ id: "650ad4eb4e6cf18f78d4fc71", image: donut6 },
		{ id: "650ad5054e6cf18f78d4ffc4", image: normalcookie6 },
		{ id: "650ad51d4e6cf18f78d50320", image: SberryDonut },
		{ id: "650ad5344e6cf18f78d50673", image: strawberryshortcake },
		{ id: "650ad5534e6cf18f78d50b13", image: strawblueberryCake },
		{ id: "650ad56e4e6cf18f78d50f73", image: weddingcake },
	  ];

	

	

/* 	function getImage() {
	  axios.post("https://cpston2-ecommerceapi-estacio.onrender.com/images/get-image", {
		productName: name
	  }, {
		headers: {
		  "Content-Type": "application/json",
		  'Authorization': `Bearer ${token}`
		}
	  })
	  .then((response) => {
		setImage(response.image);
	  })
	  .catch((error) => {
		// Handle any errors here
		console.error(error);
	  });
	}
	

useEffect(()=>{
	getImage();
})
		   
	 */
  
	return (
	  <Container className="mt-5">
		<Row>
		<Col lg={{ span: 6 }} className='mt-lg-5 ms-lg-auto' >
			<img src={image} height={500} width={500} className='shadowed viewImage rounded m-auto' />
		  </Col>
		  <Col lg={{ span: 6, }} className='mt-lg-5 me-lg-auto align-self-center'>
			<Card.Body className="text-center">
			  <Card.Title className='custom-heading'>{name}</Card.Title>
			  <Card.Subtitle className='cardText'>Description:</Card.Subtitle>
			  <Card.Text className='cardText'>{description}</Card.Text>
			  <Card.Subtitle className='cardText'>Price</Card.Subtitle>
			  <Card.Text className='cardText'>{str}{price}</Card.Text>
			  {localStorage.getItem("token") !== null ? (
				<AddToCart productId={productId} quant={currentQuantity} cartArray={cartData}/>
			  ) : 
			  (
				<Link to="/login">
				  <Button variant="success">Login to Order</Button>
				</Link>
			  )	}
			</Card.Body>
		  </Col>
		</Row>
	  </Container>
	);
  }