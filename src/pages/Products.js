import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import AdminView from "../components/admin/AdminView";
import UserView from "../components/user/UserView";
import { useCartData } from "../CartContext";


export default function Products () {
    const { user } = useContext(UserContext);
    const [activeProducts, setActiveProducts] = useState([]);
	const [products, setProducts] = useState([]);
	const cartData = useCartData();

    const fetchActiveData = () => {
	
		fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/getAllActiveProducts`)
		.then(res => res.json())
		.then(data => {
		  console.log(data);
		  if (Array.isArray(data)) {
			setActiveProducts(data);
		  } else {
			// Handle the case where data is not an array
			console.error("API response is not an array:", data);
		  }
		})
		.catch(error => {
		  console.error("Error fetching data:", error);
		});
	}

	const fetchData = () => {
		const token = localStorage.getItem('token');
	  
		fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/getAllProducts`, {
		  method: 'GET',
		  headers: {
			'Authorization': `Bearer ${token}`,
			'Content-Type': 'application/json', 
		  },
		})
		  .then(res => res.json())
		  .then(data => {
			console.log(data);
			if (Array.isArray(data)) {
			  setProducts(data);
			} else {
			  console.error("API response is not an array:", data);
			}
		  })
		  .catch(error => {
			console.error("Error fetching data:", error);
		  });
	  }
	  



	useEffect(() =>{
		fetchActiveData();
		fetchData();
	}, []);

	

    return (
        <>
	
		{
			(user.isAdmin) 
			?
			<AdminView productsData={products} fetchData={fetchData} />
			:
			<UserView productsData={activeProducts} fetchActiveData={fetchActiveData} />
		}
	
        </>
    )
}