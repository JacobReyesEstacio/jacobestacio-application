// https://cpston2-ecommerceapi-estacio.onrender.com/
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function () {



    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(true);









    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();
        fetch('https://cpston2-ecommerceapi-estacio.onrender.com/users/login',{
    
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
    
            email: email,
            password: password
    
        })
    })
    .then(res => res.json())
    .then(data => {
    
        //console.log(data.access)
        console.log("from login")
    
        if(typeof data.access !== "undefined"){
    
            localStorage.setItem('token', data.access);
    
            // function for retrieving details
            retrieveUserDetails(data.access);
    
            setUser({
                access: localStorage.getItem('token')
            })
    
            //alert(`Logged in Successfully!`);
            Swal.fire({
                title: "Login successful",
                icon: "success",
                text: "Welcome to our store!"
            })
            
        } else {
    
            //alert(`Please try again later.`)
            Swal.fire({
                title: "Authentication failed",
                icon: "error",
                text: "Check your login details and try again"
            })
        }
    })
    setEmail('');
    setPassword('');
    }







    const retrieveUserDetails = (token) => {
        fetch('https://cpston2-ecommerceapi-estacio.onrender.com/users/details', {
            headers: {
                Authorization: `Bearer ${ token } `
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }





    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);



        return (
            (user.id !== null)
            ?
            <Navigate to ="/" />
            :
            
            <Form className='offset-md-4 col-12 col-md-4 custom-heading' onSubmit={(e)=>authenticate(e)}>
            <h1 className='my-5 text-center custom-heading'>Login</h1>
            <Form.Group controlId="userEmail">
                <Form.Label><b className='custom-heading'>Email address</b></Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label className='mt-3'><b className='custom-heading'>Password</b></Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="dark" type="submit" id="submitBtn" className='my-3'>
                    Submit
                </Button>
                : 
                <Button variant="secondary" type="submit" id="submitBtn" className='my-3' disabled>
                    Submit
                </Button>
            }
            <h2 className='my-5 text-center'>Admin Access</h2>
            <h4 className='my-3 text-center'>Email: admin@gmail.com</h4>
            <h4 className='my-3 text-center'>Password: admin123</h4>
            </Form>
        )
}