import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import AdminOrders from "../components/admin/AdminOrders";
import UserOrders from "../components/user/UserOrders";

export default function Orders() {
  const { user } = useContext(UserContext);
  const [orderData, setOrderData] = useState([]);
  const [userOrderData, setUserOrderData ] = useState([]);

  const fetchUserOrderData = () => {
    const token = localStorage.getItem("token");

    fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/orders/getOrders`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (Array.isArray(data)) {
          setUserOrderData(data);
        } else {
          console.error("API response is not an array:", data);
        }
      })
      .catch((error) => {
        console.error("Error fetching OrderData:", error);
      });
  };

  const fetchOrderData = () => {
    const token = localStorage.getItem("token");

    fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/orders/orders`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (Array.isArray(data)) {
          setOrderData(data);
        } else {
          console.error("API response is not an array:", data);
        }
      })
      .catch((error) => {
        console.error("Error fetching OrderData:", error);
      });
  };

  useEffect(() => {
    fetchOrderData();
    fetchUserOrderData();
  }, []);

  return (
    <>
      
        {user.isAdmin ? (
          <AdminOrders ordersData={orderData} />
        ) : (
          <UserOrders userOrderData={userOrderData} />
        )}
      
    </>
  );
}
