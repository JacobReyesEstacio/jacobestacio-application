import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	const { unsetUser, setUser } = useContext(UserContext);

	const handleReload = () => {
		window.location.reload();
	  };

	unsetUser();
	
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		});
		handleReload();
	})

	return(
		<Navigate to="/login"/>
	)
}