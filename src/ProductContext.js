import { createContext, useContext, useState, useEffect } from 'react';
// Create a context
const ProductContext = createContext();

// Create a provider component
export function ProductProvider({ children }) {
  const [productData, setProductData] = useState([]);



  useEffect(() => {
    fetchAvailProduct();
  }, [productData]); 

  const fetchAvailProduct = () => {
    const token = localStorage.getItem('token');

    fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/getAllActiveProducts`)
		.then(res => res.json())
		.then(data => {
		  console.log(data);
		  if (Array.isArray(data)) {
			setProductData(data);
		  } else {
			// Handle the case where data is not an array
			console.error("API response is not an array:", data);
		  }
		})
		.catch(error => {
		  console.error("Error fetching data:", error);
		});
  };

  return (
    <ProductContext.Provider value={productData}> 
      {children}
    </ProductContext.Provider>
  );
}

// Create a custom hook to access the Product data
export function useProductData() {
  return useContext(ProductContext);
}

