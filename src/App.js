import { useEffect, useContext, useState } from "react";
import AppNavbar from "./components/AppNavbar";
import { UserProvider } from "./UserContext";
import { CartProvider } from "./CartContext";
import { ProductProvider } from "./ProductContext";
import { BrowserRouter as Router } from "react-router-dom";
import { Routes, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Home from "./pages/Home";
import Logout from "./pages/Logout";
import Products from "./pages/Products";
import Orders from "./pages/Orders";
import AddProduct from "./components/AddProduct";
import ProductView from "./pages/ProductView";
import './App.css';
import Error from "./pages/Error";
import AllUsers from "./components/admin/AllUsers";
import AppFooter from "./components/AppFooter";
import ScrollToTop from "./components/ScrollToTop";


function App() {

const [user, setUser] = useState({
  id : null,
  isAdmin : null
})

const unsetUser = () => {
  localStorage.clear();
}



useEffect (()=> {
  fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/users/info`,{
    headers: {
      Authorization: `Bearer ${ localStorage.getItem('token')}`
    }
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)
    if (typeof data._id !== "undefined") {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    }
    else{
      setUser({
        id: null,
        isAdmin: null
      })
    } 
  })   
}, [user])




  return (
  <div className="App background-image" >
  
  <UserProvider value={{user, setUser, unsetUser}}>
  
  <CartProvider>
  <ProductProvider>
  <Router>
  <ScrollToTop />
  <AppNavbar />
   
    <Container fluid className="wrapper" >
        
    
    
      <Routes>
      <Route path="/" element={<Home />} />
     
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/products" element={<Products />} />
        <Route path="/orders" element={<Orders />} />
        <Route path="/users" element={<AllUsers />} />
        <Route path="/addProduct" element={<AddProduct />} />
        <Route exact path="/products/:productId" element={<ProductView />} />
        <Route path="*" element={<Error/>} />


      </Routes>
      
    </Container>
    <AppFooter />
  
  </Router> 
  </ProductProvider>
  </CartProvider>
 
</UserProvider>  
</div>
  );
}

export default App;
