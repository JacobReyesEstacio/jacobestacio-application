import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


function Checkout({ cartArray }) {

	const clearCart = () => {
		fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/carts/clearCart`, {
			method: 'DELETE',
			headers: {
			  'Content-Type': 'application/json',
			  Authorization: `Bearer ${localStorage.getItem('token')}`,
			}
		  })
			.then((res) => res.text())
			.then((responseText) => {
			  if (responseText === 'Product removed from cart') {
				console.log("cleared cart")
			  } else {
				Swal.fire({
				  title: 'Clear Cart failed',
				  icon: 'info',
				  text: 'Please Login to Order.',
				});
			  }
			})
			.catch((error) => {
			  console.error(error);
			  Swal.fire({
				title: 'Something went wrong',
				icon: 'error',
				text: 'Please try again.',
			  });
			});
	}


	
  const checkOut = () => {
	
    // Map the cart items to orderProducts array
    const orderProducts = cartArray.map((item) => ({
      productId: item.productId,
      quantity: item.quantity,
    }));

	if (orderProducts.length === 0) {
		Swal.fire({
			title: 'Order failed',
			icon: 'info',
			text: 'Your Cart is Empty.',
		  });
	}
	else {
		fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/orders/checkout`, {
			method: 'POST',
			headers: {
			  'Content-Type': 'application/json',
			  Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
			body: JSON.stringify({
			  orderProducts: orderProducts,
			}),
		  })
			.then((res) => res.text())
			.then((responseText) => {
			  if (responseText === 'Order placed successfully!') {
				Swal.fire({
				  title: 'Successfully ordered',
				  icon: 'success',
				  text: 'You have successfully ordered the product.',
				});
				clearCart();
			  } else {
				Swal.fire({
				  title: 'Order failed',
				  icon: 'info',
				  text: 'Please Login to Order.',
				});
			  }
			})
			.catch((error) => {
			  console.error(error);
			  Swal.fire({
				title: 'Something went wrong',
				icon: 'error',
				text: 'Please try again.',
			  });
			});
	}

   
  };

  return (
    <div>
      <Button variant='success' className='checkout-button shadowed' onClick={checkOut}>
        Checkout
      </Button>
    </div>
  );
}

export default Checkout;
