import React, { useState, useEffect, useRef } from 'react';
import { SideBarData } from './SideBarData';
import { Link } from 'react-router-dom';
import { Navbar } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRightToBracket } from '@fortawesome/free-solid-svg-icons';

function SideBar() {
  const [isOpen, setIsOpen] = useState(false);
  const sidebarRef = useRef(null);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    const handleDocumentClick = (event) => {
      if (sidebarRef.current && !sidebarRef.current.contains(event.target)) {
        setIsOpen(false);
      }
    };

    document.addEventListener('click', handleDocumentClick);

    return () => {
      document.removeEventListener('click', handleDocumentClick);
    };
  }, []);

  return (
    <>
      <Navbar expand="lg" className="Sidebar">
        <Navbar.Collapse id="sidebar-nav">
          <div className="sideBarList">
            {SideBarData.map((item, key) => {
              return (
                <div className="sidePanel" key={key}>
                  {item.icon}
                  <Link to={item.link}>
                    <button className="sideBarButton">{item.title}</button>
                  </Link>
                </div>
              );
            })}
          </div>
        </Navbar.Collapse>
      </Navbar>

      <div
        ref={sidebarRef}
        className={`off-canvas-menu ${isOpen ? 'open' : ''}`}
      >
        <button
          className={`d-lg-none ${isOpen ? 'menu-toggle-hidden' : 'menu-toggle'}`}
          onClick={toggleMenu}
        >
          <FontAwesomeIcon icon={faRightToBracket} />
        </button>
        <ul className="menu-list">
          {SideBarData.map((item, key) => {
            return (
              <li key={key}>
                
                <Link className='sideBarLi' to={item.link}>{item.title}</Link>
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
}

export default SideBar;
