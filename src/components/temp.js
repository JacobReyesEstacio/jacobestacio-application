import { useState, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Layout from './Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCirclePlus } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios'; // Import Axios

export default function AddProduct() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  // Input states
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [image, setImage] = useState(null); // Add state for the image

  function createProduct(e) {
    e.preventDefault();

    const formData = new FormData();
    formData.append('productName', name);
    formData.append('productDescription', description);
    formData.append('productPrice', price);
    formData.append('image', image); // Add the image to the form data

    let token = localStorage.getItem('token');
    console.log(token);

    axios
      .post('https://cpston2-ecommerceapi-estacio.onrender.com/products/create-product', formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'multipart/form-data', // Set the content type to multipart/form-data for file uploads
        },
      })
      .then((response) => {
        const data = response.data;
        console.log(data);

        if (data) {
          Swal.fire({
            icon: 'success',
            title: 'Product Added',
          });

          navigate('/products');
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Unsuccessful Product Creation',
            text: data.message,
          });
        }
      })
      .catch((error) => {
        console.error(error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'An error occurred while creating the product.',
        });
      });
  }

  return (
    <Layout>
      <Container fluid>
        <Row className="justify-content-md-center">
          <Col sm={10} md={9}>
            <h1 className="pt-5 pb-3 text-center custom-heading">
              <FontAwesomeIcon className="mx-2" icon={faCirclePlus} /> Add Product
            </h1>
            <Form
              size-lg="sm"
              size-sm="lg"
              className="shadowed bg-light text-dark p-3 rounded custom-heading"
              onSubmit={(e) => createProduct(e)}
            >
              {[
                { label: 'Name', type: 'text', value: name, onChange: setName },
                { label: 'Description', type: 'text', value: description, onChange: setDescription },
                { label: 'Price', type: 'number', value: price, onChange: setPrice },
                // Add input for image file
                { label: 'Image', type: 'file', accept: 'image/*', onChange: (file) => setImage(file) },
              ].map((field, index) => (
                <Form.Group className="py-2" key={index}>
                  <Form.Label>{field.label}:</Form.Label>
                  {field.type === 'file' ? (
                    <Form.File
                      accept={field.accept}
                      custom
                      onChange={(e) => field.onChange(e.target.files[0])}
                    />
                  ) : (
                    <Form.Control
                      type={field.type}
                      placeholder={`Enter ${field.label}`}
                      required
                      value={field.value}
                      onChange={(e) => field.onChange(e.target.value)}
                    />
                  )}
                </Form.Group>
              ))}
              <Button variant="danger" type="submit" className="m-3">
                Confirm
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
}