import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faTwitter,
  faFacebook,
  faInstagram,
  faTiktok,
  faCcVisa,
  faCcMastercard,
  faCcPaypal,
  faCcAmex,
} from '@fortawesome/free-brands-svg-icons';

export default function AppFooter() {
  return (
    <footer className="mt-5">
      <Container>
        <Row>
          <Col>
            <p>&copy; 2023 Pâtisserie Flourique</p>
          </Col>
        </Row>
        <Row className="justify-content-center">
         
          <Col xs={6} sm={6} md={4}>
            <div className="d-flex justify-content-center">
              <a href="https://www.facebook.com" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="mx-2 custom-icon-color" icon={faFacebook} />
              </a>
              <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="mx-2 custom-icon-color" icon={faTwitter} />
              </a>
              <a href="https://www.instagram.com" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="mx-2 custom-icon-color" icon={faInstagram} />
              </a>
              <a href="https://www.tiktok.com" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="mx-2 custom-icon-color" icon={faTiktok} />
              </a>
            </div>
          </Col>

       
          <Col xs={6} sm={6} md={4}>
            <div className="d-flex justify-content-center">
            
              <FontAwesomeIcon className="mx-2 custom-icon-color" icon={faCcVisa} />
              <FontAwesomeIcon className="mx-2 custom-icon-color" icon={faCcMastercard} />
              <FontAwesomeIcon className="mx-2 custom-icon-color" icon={faCcPaypal} />
              <FontAwesomeIcon className="mx-2 custom-icon-color" icon={faCcAmex} />
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
}
