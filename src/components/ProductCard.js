import React from "react";
import { Card, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import caramellemoncake from "../images/caramellemoncake.jpg";
import chococake from "../images/chococake.jpg"
import chococookie6 from "../images/chococookie6.jpg";
import chocomousecake from "../images/chocomousecake.jpg";
import classicdonut12 from "../images/classicdonut12.jpg";
import crepecake from "../images/crepecake.jpg";
import donut6 from "../images/donut6.jpg";
import normalcookie6 from "../images/normalcookie6.jpg";
import SberryDonut from "../images/SberryDonut.jpg";
import strawberryshortcake from "../images/strawberryshortcake.jpg";
import strawblueberryCake from "../images/strawblueberryCake.jpg";
import weddingcake from "../images/weddingcake.jpg"
import general from "../images/general.jpg";

export default function ProductCard(props) {
  let str = "\u20B1";
  const product = props.product;
  const token = localStorage.getItem("token");
  const [image, setImage] = useState(null)

 /*  function getImage() {
    fetch("http://localhost:4000/images/get-image", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify ({
        productName: product.productName
      })
    })
      .then((res) => res.json())
      .then((data) => {
        setImage(data.image)
      })
      
  }
  
 */

  const getImagePath = (productId) => {
    const productImage = imageArray.find((item) => item.id === productId);
    return productImage ? productImage.image : general;
  };

  useEffect(() => {
    const imagePath = getImagePath(product._id);
   setImage(imagePath);
  }, [product.product_id]);

 const imageArray = [
    { id: "650ad4444e6cf18f78d4edf8", image: caramellemoncake },
    { id: "650ad45d4e6cf18f78d4ef50", image: chococake },
    { id: "650ad4804e6cf18f78d4f19a", image: chococookie6 },
    { id: "650ad4994e6cf18f78d4f39b", image: chocomousecake },
    { id: "650ad4b84e6cf18f78d4f69c", image: classicdonut12 },
    { id: "650ad4ce4e6cf18f78d4f904", image: crepecake },
    { id: "650ad4eb4e6cf18f78d4fc71", image: donut6 },
    { id: "650ad5054e6cf18f78d4ffc4", image: normalcookie6 },
    { id: "650ad51d4e6cf18f78d50320", image: SberryDonut },
    { id: "650ad5344e6cf18f78d50673", image: strawberryshortcake },
    { id: "650ad5534e6cf18f78d50b13", image: strawblueberryCake },
    { id: "650ad56e4e6cf18f78d50f73", image: weddingcake },
  ];





  return (
    <>
              <div className="d-flex justify-content-center cardContainer">
                   <Card className="h-100 p-3 align-items-center cardMain"  style={{ gap: ".2rem" }} 
                    as={Link} to={`/products/${product._id}`}>
                   <Card.Img variant="top" className="view-image" src={image} height={"200px"} style={{ objectFit: "cover" }} />
                    <Card.Body className="d-flex flex-column">
                    <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
                    <span className="fs-7 card-text">{product.productName}</span>
                    <span className="ms-5 card-price">{str}
                    {product.productPrice}</span></Card.Title>                    
                  </Card.Body>
                </Card>
                </div>
    </>
  );
}
