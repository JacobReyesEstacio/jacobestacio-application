import React, { useState, useEffect } from 'react';
import { Table, Container, Modal, Button } from 'react-bootstrap';
import Layout from '../Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUserTag,
  faCoins,
  faCalendarDays,
  faSackDollar,
  faUserGear,
} from '@fortawesome/free-solid-svg-icons';
import { useProductData } from '../../ProductContext';

let str = "\u20B1";

const AdminOrders = ({ ordersData }) => {
  const [orders, setOrders] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState(null);
  const productData = useProductData();

  useEffect(() => {
    const ordersArr = ordersData.map((order) => {
      return (
        <tr key={order._id} onClick={() => handleOrderClick(order)}>
          <td>{order._id}</td>
          <td className="d-none d-md-table-cell">{order.orderUserId}</td>
          <td className="d-none d-md-table-cell">{str}{order.totalAmount}</td>
          <td className="d-none d-md-table-cell">{order.purchasedOn}</td>
        </tr>
      );
    });
    setOrders(ordersArr);
  }, [ordersData]);

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  const handleOrderClick = (order) => {
    setSelectedOrder(order);
    toggleModal();
  };

  return (
    <Layout>
      <Container fluid>
        
          <h1 className="my-5 text-center custom-heading">
            <FontAwesomeIcon className="mx-2" icon={faUserGear} /> Admin Dashboard
          </h1>

          <div
            className="table-responsive"
            style={{
              overflowX: 'auto',
              overflowY: 'auto',
              maxHeight: '500px',
              maxWidth: '1100px',
            }}
          >
            <Table striped bordered hover responsive="sm" className="table table-secondary shadowed">
              <thead>
                <tr>
                  <th className={`table-dark`}>
                    <FontAwesomeIcon className="mx-2" icon={faSackDollar} />Sale ID "Click IDs for more details"
                  </th>
                  <th className="table-dark d-none d-md-table-cell">
                    <FontAwesomeIcon className="mx-2" icon={faUserTag} />User ID
                  </th>
                  <th className="table-dark d-none d-md-table-cell">
                    <FontAwesomeIcon className="mx-2" icon={faCoins} />Amount
                  </th>
                  <th className="table-dark d-none d-md-table-cell">
                    <FontAwesomeIcon className="mx-2" icon={faCalendarDays} />Date
                  </th>
                </tr>
              </thead>
              <tbody>{orders}</tbody>
            </Table>
          </div>

          <Modal show={showModal} onHide={toggleModal}>
            <Modal.Header closeButton>
              <Modal.Title>Order Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {selectedOrder && (
                <div>
                  <h5>Order ID: {selectedOrder._id}</h5>
                  <h5>User ID: {selectedOrder.orderUserId}</h5>
                  <h5>Total Amount: {str}{selectedOrder.totalAmount}</h5>
                  <h5>Purchased On: {selectedOrder.purchasedOn}</h5>
                  <h5>Ordered Products:</h5>
                  <ul>
                    {selectedOrder.orderProducts.map((product) => {
                      const productInfo = productData.find((data) => data._id === product.productId);
                      const productName = productInfo ? productInfo.productName : 'Product Not Found';

                      return (
                        <li key={product.productId}>
                          Product Name: {productName}, Quantity: {product.quantity}
                        </li>
                      );
                    })}
                  </ul>
                </div>
              )}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={toggleModal}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
        
      </Container>
    </Layout>
  );
};

export default AdminOrders;
