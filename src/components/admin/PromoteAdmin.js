import React, { useState } from "react";
import Swal from "sweetalert2";

export default function PromoteAdmin({ userId, isAdmin }) {
  const [promotionId] = useState(userId);

  const promote = () => {
    fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/users/setAdmin`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        id: promotionId, // Convert to JSON string
      }),
    })
      .then((res) => res.text())
      .then((data) => {
        console.log(data);
        if (data === "admin promotion success!") {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "User successfully promoted!",
          }).then(() => {
            // Handle success, if needed
          });
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "Failed to promote. Please try again.",
          });
        }
      });
  };

  return (
    <div>
      {isAdmin ? (
        <></>
      ) : (
        <button className="btn btn-danger shadowed" onClick={promote}>
          Promote
        </button>
      )}
    </div>
  );
}
