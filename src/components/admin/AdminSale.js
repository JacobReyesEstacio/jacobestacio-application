import { useState } from 'react';
import { Button, Modal, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminSale() {
  const [sale, setSale] = useState([]);
  const [showView, setShowView] = useState(false);
  const [userId, setUserId] = useState(null); // To keep track of the user for which orders are displayed in the modal

  const openView = (userId) => {
    fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/orders/sale/${userId}`)
      .then((res) => res.json())
      .then((data) => {
        setSale(data);
        setShowView(true);
      })
      .catch((error) => {
        console.error(error);
        // Handle error here (e.g., show an error message)
      });
    setUserId(userId);
  };

  const closeView = () => {
    setShowView(false);
    setUserId(null); // Reset the user ID when the modal is closed
  };

  return (
    <>
        Will be fixed soon
    </>
  );
}
