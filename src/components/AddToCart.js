import React, { useContext, useState, useEffect } from "react";
import Swal from "sweetalert2";





function AddToCart({productId, quant, cartArray}) {

const [quantt, setQuantt] = useState(quant)
    const addToCart = () => {

      fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/carts/addToCart`, {
        method: 'POST',
        headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          productId: productId,
          quantity: 1 
        })
      })
      .then(res => res.text())
      .then(data => {
        if (data === "Product added to cart") {
        Swal.fire({
          title: "Successfully ordered",
          icon: 'success',
          text: "You have successfully ordered the product."
        });
        setQuantt(1)
        } else {
        Swal.fire({
          title: "Order failed",
          icon: 'info',
          text: "Please Login to Order."
        });
    
        }
      })
      .catch(error => {
        console.error(error);
        Swal.fire({
        title: "Something went wrong",
        icon: 'error',
        text: "Please try again."
        });
      });
      }


      const incrementCart = () => {
        const cartItem = cartArray.find(item => item.productId === productId);
        const updatedQuantity = cartItem.quantity + 1;
    
        fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/carts/updateCart`, {
          method: 'PUT',
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            productId: productId,
            quantity: updatedQuantity // Use the updated quantity directly
          })
        })
        .then(res => res.text())
        .then(data => {
          if (data === "Cart updated") {
            console.log("success");
          } else {
              console.log("failed update");
          }
        })
        .catch(error => {
          console.error(error);
          Swal.fire({
            title: "Something went wrong",
            icon: 'error',
            text: "Please try again."
          });
        });
      }



  return (
    <div>
            { quantt == 0 ?(
            <button className="orderButton shadowed" onClick={addToCart}>Add to Cart</button>)
             : (
              <button className="orderButton shadowed" onClick={incrementCart}>Add to Cart</button>)
            }
    </div>
  )
}

export default AddToCart
