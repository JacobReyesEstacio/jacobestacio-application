import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { Row, Col } from "react-bootstrap";



function UpdateCart({ productId, cartArray }) {
  const [currentQuantity, setCurrentQuantity] = useState(0); 

  useEffect(() => {
    const cartItem = cartArray.find((item) => item.productId === productId);
    setCurrentQuantity(cartItem ? cartItem.quantity : 0); // Set the initial quantity
  }, [cartArray, productId]);

    const incrementCart = () => {
      const cartItem = cartArray.find(item => item.productId === productId);
      const updatedQuantity = cartItem.quantity + 1;
  
      fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/carts/updateCart`, {
        method: 'PUT',
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          productId: productId,
          quantity: updatedQuantity // Use the updated quantity directly
        })
      })
      .then(res => res.text())
      .then(data => {
        if (data === "Cart updated") {
          console.log("success");
        } else {
            console.log("failed update");
        }
      })
      .catch(error => {
        console.error(error);
        Swal.fire({
          title: "Something went wrong",
          icon: 'error',
          text: "Please try again."
        });
      });
    }


    const decrementCart = () => {
        const cartItem = cartArray.find(item => item.productId === productId);
        const updatedQuantity = cartItem.quantity -1;
    
        fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/carts/updateCart`, {
          method: 'PUT',
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            productId: productId,
            quantity: updatedQuantity // Use the updated quantity directly
          })
        })
        .then(res => res.text())
        .then(data => {
          if (data === "Cart updated") {
            console.log("success");
          } else {
            console.log("failed update");
          }
        })
        .catch(error => {
          console.error(error);
          Swal.fire({
            title: "Something went wrong",
            icon: 'error',
            text: "Please try again."
          });
        });
      }



      const removeFromCart = () => {
        const cartItem = cartArray.find(item => item.productId === productId);
        const updatedQuantity = cartItem.quantity -1;
    
        fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/carts/removeCart`, {
          method: 'DELETE',
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            productId: productId,
          })
        })
        .then(res => res.text())
        .then(data => {
          if (data === "Cart updated") {
            console.log("success");
          } else {
            console.log("failed update");
          }
        })
        .catch(error => {
          console.error(error);
          Swal.fire({
            title: "Something went wrong",
            icon: 'error',
            text: "Please try again."
          });
        });
      }
  
    return (
      <Row>
      <Col>
        <button className="orderButton shadowed smaller-button" onClick={incrementCart}>
          +
        </button>
        {currentQuantity > 1 ? (
          <button className="orderButton shadowed smaller-button" onClick={decrementCart}>
            -
          </button>
        ) : (
          <button className="shadowed smaller-button" onClick={removeFromCart}>
            -
          </button>
        )}
      </Col>
      <Col>
        <button className="shadowed smaller-button" onClick={removeFromCart}>
          Remove
        </button>
      </Col>
    </Row>
    
    )
  }

export default UpdateCart
  