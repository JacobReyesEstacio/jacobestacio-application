import React from "react";
import Swal from "sweetalert2";

export default function ArchiveProducts({ productId, isActive, fetchData }) {

  const archiveToggle = () => {
    fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/archiveProduct/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "Product successfully archived!",
          }).then(() => {
            fetchData();
          });
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "Failed to archive Product. Please try again.",
          });
        }
      });
  };

  const activateToggle = () => {
    fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/activateProduct/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "Product successfully archived!",
          }).then(() => {
            fetchData();
          });
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "Failed to archive Product. Please try again.",
          });
        }
      });
  };

  return (
    <div>
      {isActive ? (
        <button className="btn btn-info shadowed" onClick={archiveToggle}>
          Archive
        </button>
      ) : (
        <button className="btn btn-dark shadowed" onClick={activateToggle}>
          Activate
        </button>
      )}
    </div>
  );
}