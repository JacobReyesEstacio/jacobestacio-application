import { useState, useEffect } from 'react';
import { Table, Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarDays, faCartShopping, faCoins } from '@fortawesome/free-solid-svg-icons';
import { useProductData } from '../../ProductContext';



let str = "\u20B1";
const UserOrders = ({ userOrderData }) => {
    const [orders, setOrders] = useState([]);
    const productData = useProductData();
    
    
    useEffect(() => {
        const ordersArr = userOrderData.map((order) => (
            <tr key={order._id}>
                <td>
                <Row>
                        <Col>
                            <p className='gray-text'>Products:</p>
                        </Col>
                        <Col>
                            <p className='gray-text'>Quantity:</p>
                        </Col>
                    </Row>
                    {order.orderProducts.map((product) => (
                        <Row key={product.productId}>
                            <Col>
                            <p>{productData.find((p) => p._id === product.productId)?.productName}</p>
                            </Col>
                            <Col>
                                <p>{product.quantity}</p>
                            </Col>
                        </Row>
                    ))}
                </td>
                <td>{str}{order.totalAmount}</td>
                <td>{order.purchasedOn}</td>
            </tr>
        ));
        setOrders(ordersArr);
    }, [userOrderData]);

    return (
       
        <Container>
            <h1 className='my-5 text-center custom-heading'> Order History</h1>
            <div className="table-responsive ms-lg-5 ps-lg-5" style={{ overflowX: 'auto', overflowY: 'auto', maxHeight: '500px', maxWidth: '1100px' }}>
            <Table striped bordered hover responsive="sm" className=' table-secondary shadowed'>
                <thead>
                    <tr>
                        <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faCartShopping} />Orders</th>
                        <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faCoins} />Amount</th>
                        <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faCalendarDays} />Date</th>
                    </tr>
                </thead>
                <tbody>{orders}</tbody>
            </Table>
            </div>
        </Container>
       
    );
}

export default UserOrders;
