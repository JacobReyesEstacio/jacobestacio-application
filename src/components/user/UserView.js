import React from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import ProductCard from '../ProductCard'


const UserView = ({productsData, fetchActiveData}) => {

   

  return (
    
<Container>
    <h1 align="center" className='p-1 my-5  custom-heading'>Our Products</h1>
    
    <Row noGutters lg={4}>
        {productsData.map((product, idx)=> (
            <Col align="center" key={idx}>
                <ProductCard product={product} fetchActiveData={fetchActiveData} />
            </Col>
        ))}
    </Row>
    
</Container>
  )
}

export default UserView
