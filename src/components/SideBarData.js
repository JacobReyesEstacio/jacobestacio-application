import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPersonWalkingLuggage, faBreadSlice, faPlus, faBagShopping } from '@fortawesome/free-solid-svg-icons';
import '../App.css'
export const SideBarData = [
    {
        title: "Users",
        icon: <FontAwesomeIcon icon={faPersonWalkingLuggage} />,
        link: "/users"
    },
    {
        title: "Products",
        icon: <FontAwesomeIcon icon={faBreadSlice} />,
        link: "/products"
    },
    {
        title: "Add Product",
        icon: <FontAwesomeIcon icon={faPlus}  />,
        link: "/addProduct"
    },
    {
        title: "Orders",
        icon: <FontAwesomeIcon icon={faBagShopping} />,
        link: "/orders"
    },
];
 


