import { useContext, useState, useEffect } from "react";
import { Navbar, Nav, Container, Button, Modal, Row, Col } from "react-bootstrap";
import { Link, NavLink } from 'react-router-dom';
import UserContext from "../UserContext";
import { useCartData } from "../CartContext";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartShopping, faHome, faWheatAwn } from '@fortawesome/free-solid-svg-icons';
import UpdateCart from "./UpdateCart";
import { useProductData } from "../ProductContext";
import Checkout from "./Checkout";




export default function AppNavbar () {
  let str = "\u20B1";

    const { user } = useContext(UserContext);
    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);
    const cartData = useCartData();
    const productData = useProductData();
   

      const [itemPrice, setItemPrice] = useState([]);
      const [totalPrice, setTotalPrice] = useState([]); 
      const [productName, setProductName] = useState([])
      const [grandTotal, setGrandTotal] = useState();
      
       useEffect(() => {
        calculatePrices();
    }, [cartData]); 

     const calculatePrices = () => {
      const newPrice = {};
      const newTotalPrice = {};
      const newProductName = {};
      cartData.forEach((cartItem) => {
          // Find the product if it exists in productData
          const item = productData.find((product) => product._id === cartItem.productId);
          
          if (item) {
              newPrice[cartItem.productId] = item.productPrice;
              newTotalPrice[cartItem.productId] = item.productPrice * cartItem.quantity;
              newProductName[cartItem.productId] = item.productName;
          } else {
              // Handle the case where the product is not found
              newPrice[cartItem.productId] = 0; // or any default value
              newTotalPrice[cartItem.productId] = 0; // or any default value
          }
      });
  
      setItemPrice(newPrice);
      setTotalPrice(newTotalPrice);
      setProductName(newProductName);
      const totalSum = Object.values(totalPrice).reduce((acc, price) => acc + price, 0);
      setGrandTotal(totalSum);
  };
 
   

  

    return(
        <>
        <Navbar className="navMain" expand="lg" data-bs-theme="dark">
        <Container>
            <Navbar.Brand className="custom-heading" as={Link} to="/">
            <FontAwesomeIcon className='mx-2' icon={faWheatAwn} />
                  Pâtisserie Flourique
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" align="center">
            <Nav className="ms-auto">
					   <Nav.Link className="dropdown-custom" as={Link} to="/"> <FontAwesomeIcon className='me-2 dropdown-custom' icon={faHome} />Home</Nav.Link>
					   <Nav.Link className="dropdown-custom" as={Link} to="/products">
                       {
                        (user.isAdmin) ? "Dashboard"
                        : "Products"
                       }
                       
                        </Nav.Link>
					            	{
                            (user.id !== null) ?

                            user.isAdmin
                            ?
                            <>
						            <Nav.Link className="dropdown-custom" as={Link} to="/logout">Logout</Nav.Link>
                            </>
                            :
                            <>
                                    <Nav.Link className="dropdown-custom" as={Link} to="orders">Orders</Nav.Link>
						                        <Nav.Link className="dropdown-custom" as={Link} to="/logout">Logout</Nav.Link>
                                    <button className="ms-3 cartButton" onClick={handleShow}>
                                    <FontAwesomeIcon className='mx-2' icon={faCartShopping} />
                                    Cart {cartData.length}</button>
                            </>
                            :
                            <>
                                   <Nav.Link className="dropdown-custom" as={Link} to="/login">Login</Nav.Link>
						                       <Nav.Link className="dropdown-custom" as={Link} to="/register">Register</Nav.Link>
                            </>
                        }
						            
						        
						               
						        
						              
					   </Nav>
            </Navbar.Collapse>
            </Container>
          </Navbar>
      
              <Modal className="cartModal" show={show} onHide={handleClose}>
            <Modal.Header>
            <Modal.Title className="custom-heading">
            <FontAwesomeIcon className='mx-2' icon={faCartShopping} />
            Your Cart</Modal.Title>
            </Modal.Header>
          
         
            <Modal.Body className="cart-back shadowed">
              <div className="cart-item-list">
             {   grandTotal === 0 ? (
            <div><h1 className="custom-heading-cart">Empty Cart</h1></div>
          ) : (
                cartData.map((cartItem, idx) => (
                  <div key={idx} className="cart-item">
                    <h4 className="cart-font">{productName[cartItem.productId]}</h4>
                    <p>Quantity: {cartItem.quantity}</p>
                    <UpdateCart productId={cartItem.productId} cartArray={cartData} />
                    <p>Price each: {str}{itemPrice[cartItem.productId]}</p>
                    <p>Total: {str}{totalPrice[cartItem.productId]}</p>
                  </div>
                )) )
        }
              </div>
              <div> <h3 className="grandTotal">Total: {str}{grandTotal}</h3></div>
            <div> <Checkout cartArray={cartData}/></div>
            </Modal.Body>
         
          </Modal>
       
                     

          

              </>
              )
          }