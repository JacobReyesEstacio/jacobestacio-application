import { createContext, useContext, useState, useEffect } from 'react';
import UserContext from './UserContext';
// Create a context
const CartContext = createContext();

// Create a provider component
export function CartProvider({ children }) {
  const [cartData, setCartData] = useState([]);
  const { user } = useContext(UserContext);




  useEffect(() => {
    fetchCart();
  }, [user]); 

  useEffect(() => {
    fetchCart();
  }, [cartData]); 

  const fetchCart = () => {
    const token = localStorage.getItem('token');

    fetch('https://cpston2-ecommerceapi-estacio.onrender.com/carts/allCart', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        console.log(data);
        if (Array.isArray(data)) {
          setCartData(data);
        } else {
          console.error('API response is not an array:', data);
        }
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  };



  return (
    <CartContext.Provider value={cartData}> 
      {children}
    </CartContext.Provider>
  );
}

// Create a custom hook to access the cart data
export function useCartData() {
  return useContext(CartContext);
}

